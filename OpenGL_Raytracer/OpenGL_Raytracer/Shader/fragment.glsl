#version 430

precision mediump float;

in lowp vec4 fColor;
in vec2 fTexCoords;

uniform sampler2D tex;

out vec4 outColor;

void main()
{
	outColor = texture(tex, fTexCoords);
}
