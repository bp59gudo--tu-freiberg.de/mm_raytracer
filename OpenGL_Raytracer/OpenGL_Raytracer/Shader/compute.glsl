/*#version 430
layout (local_size_x = 1, local_size_y = 1) in;                             
layout (rgba32f, binding = 0) uniform image2D img_output;                   
                                                                            
void main () {                  

	// INIT
	// Set a Standard-Color for non-Hitted Points
    vec4 pixel = vec4 (1.0, 0.0, 0.0, 1.0);  
	
	// Sphere-Attributes
	vec3 sphere_center = vec3 (0.0, 0.0, -10.0);   
    float sphere_radius = 3;                                                         
   
    // Scale-Values for our ray-ortho
    float max_x = 5.0;                                                          
    float max_y = 5.0;                                                          
    
	// That is the Position on Screen where we will draw the Pixel to, we query the Dimensions too
    ivec2 pixel_coords = ivec2 (gl_GlobalInvocationID.xy);       
	ivec2 dims = imageSize (img_output);     						

	// CALCULATION
	// Now we let the ray point to the Sphere at (0,0) and normalize it
    float x = (float(pixel_coords.x * 2 - dims.x) / dims.x);                    
    float y = (float(pixel_coords.y * 2 - dims.y) / dims.y);   
	
	// so that this here points from the current pixel_coords to the Sphere Coords in 2D
    vec3 ray_ortho = vec3 (x * max_x, y * max_y, 0.0);                              
    
	// obviously the ortho     
	vec3 ray_direction = vec3 (0.0, 0.0, -1.0);                          
                                
	// Do Sphere-Hitted?-Stuff
    vec3 ray_orthoToSphere = sphere_center - ray_ortho;                                            
    float b = dot (ray_direction, -ray_orthoToSphere);                                          
    float c = dot (ray_orthoToSphere, ray_orthoToSphere) - sphere_radius * sphere_radius;                        
    float bsqmc = b * b - c;                                                                                 
	

	// If our Sphere was hitted, bsqmc should be >= 0 so change the Color of the Pixel
	if (bsqmc >= 0.0) {                                                       
		pixel = vec4 (0.4, 0.4, 1.0, 1.0);                                   
    }

	// Check gl_GlobalInvocationID for error per 0-Check... 
	//if ( gl_GlobalInvocationID.x == 0 && gl_GlobalInvocationID.y == 0) {
	//	pixel = vec4(1.0, 1.0, 1.0, 1.0);
	//}


	// Write resulting Color
    imageStore (img_output, pixel_coords, pixel);
}

// TODO: gl_GlobalInvocationID.xy gives (0,0), but we want it to hold the current Pixel's Position!
// After that, you should check on the Hit-Calculation if it does not give the Sphere Shape.
*/

#version 430
//size of local work group
layout(local_size_x = 1, local_size_y = 1) in;
//internal image format
layout(rgba32f, binding = 0) uniform image2D img_output;

vec3 ray_d = vec3(0.0, 0.0, -1.0); // ortho
vec3 sphere_c = vec3(0.0, 0.0, -10.0);
float sphere_r = 7.07;

void main() {
  // base pixel colour for image
  vec4 pixel = vec4(1.0, 0.0, 0.0, 1.0);
  // get index in global work group i.e x,y position
  ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

  //
  // Raytracing stuff
  //

  float max_x = 5.0;
  float max_y = 5.0;
  ivec2 dims = imageSize(img_output); // fetch image dimensions
  float x = (float(pixel_coords.x * 2 - dims.x) / dims.x);
  float y = (float(pixel_coords.y * 2 - dims.y) / dims.y);
  vec3 ray_o = vec3(x * max_x, y * max_y, 0.0);
  vec3 omc = ray_o - sphere_c;
  float b = dot(ray_d, omc);
  float c = dot(omc, omc) - sphere_r * sphere_r;
  float bsqmc = b * b - c;
  // hit one or both sides
  if (bsqmc >= 0.0) {
    pixel = vec4(0.4, 0.4, 1.0, 1.0);
  }

  // output to a specific pixel in the image
  imageStore(img_output, pixel_coords, pixel);
}
