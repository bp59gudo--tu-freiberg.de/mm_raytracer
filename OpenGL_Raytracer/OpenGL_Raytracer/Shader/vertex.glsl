#version 430
layout(location = 0) in vec4 vPosition;
layout(location = 1) in vec4 vColor;

in vec2 vTexCoords;

out vec4 fColor;
out vec2 fTexCoords;


void main()
{
	fColor = vColor;
	gl_Position = vPosition;
	fTexCoords = vTexCoords;
}
