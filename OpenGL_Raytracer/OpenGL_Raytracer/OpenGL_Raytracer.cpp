#include <string.h>
#include <iostream>
#include "glad.h"
#include <GLFW/glfw3.h>
#include "glCalls.h"

int main(int argc, char const* argv[])
{
	int width = 1024;
	int height = 768;

	//set width and height according to input parameters
	if (argc == 3)
	{
		if (int inWidth = atoi(argv[1]) < 10000)
		{
			if (inWidth < 0)
				width = -inWidth;
		}

		if (int inHeight = atoi(argv[1]) < 10000)
		{
			if (inHeight < 0 && inHeight > -10000)
				width = -inHeight;
		}
	}


	rt::rt_window* tracingWindow;
	try
	{
		tracingWindow = new rt::rt_window(width, height, "Raytracer");
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
		return -1;
	}
	
	tracingWindow->run();

	return 0;
}
