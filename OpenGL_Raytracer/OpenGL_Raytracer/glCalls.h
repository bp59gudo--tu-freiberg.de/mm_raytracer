#ifndef GLCALLS_H
#define GLCALLS_H
#define _USE_MATH_DEFINES

#include "glad.h"
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <math.h>
#include <string>
#include <exception>
#include <assert.h>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#define ATTRIB_POSITION 0
#define ATTRIB_TEX 1

namespace rt
{

	class rt_window
	{
	public:
		rt_window();
		rt_window(int sizeX, int sizeY, std::string name);
		~rt_window();
		void run();

	private:
		struct VertexData
		{
			GLfloat position[3];
			GLfloat texCoords[3];
		};

		//UserData
		GLuint programObjectNonCompute;
		GLuint programObjectCompute;
		GLuint vertexBufferObject;
		GLuint vertexDataCount;
		GLuint vertexArrayObject;
		double time;

		//Variables
		GLFWwindow* window;
		int windowWidth, windowHeight;
		GLuint texture; //for raytracing
		struct VertexData* rt_texture_quad; //quad, which the texture is presented on

		//methods related to shaders
		GLuint compileShader(GLenum type, const char* shaderSource, std::string shaderTag);
		char* readShaderFromFile(std::string filename);
		GLboolean checkLinkedProgram(GLuint programObject);

		//methods related to window
		bool init(int sizeX, int sizeY, std::string name);
		void teardown();
		static void error_callback(int error, const char* description);
		void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
		void checkError(const char* errorText);

		//methods related to raytracing
		void generateTexture();
		void queryWorkGroups();
		void initComputeShader();

		//methods to run the initialized program
		void update();
		void draw();
	};
}
#endif
