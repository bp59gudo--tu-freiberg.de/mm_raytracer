#include "glCalls.h"

rt::rt_window::rt_window()
{
	if (!init(640, 480, "Raytracer"))
	{
		throw std::exception("Failed to initialize window");
	}
}

rt::rt_window::rt_window(int sizeX, int sizeY, std::string name)
{
	if (!init(sizeX, sizeY, name))
	{
		throw std::exception("Failed to initialize window");
	}
}

rt::rt_window::~rt_window()
{
	delete[] rt_texture_quad;
	teardown();
	glfwDestroyWindow(window);
	glfwTerminate();
}

void rt::rt_window::run()
{
	while (!glfwWindowShouldClose(window))
	{
		update();
		draw();
		glfwPollEvents();

		if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_ESCAPE)) {
			glfwSetWindowShouldClose(window, 1);
		}
		glfwSwapBuffers(window);
	}
}

bool rt::rt_window::init(int sizeX, int sizeY, std::string name)
{
	//register error callback
	glfwSetErrorCallback(this->error_callback);

	//try to init glfw
	if (!glfwInit())
	{
		printf("Failed to initialize GLFW\n");
		return false;
	}

	// 4x antialiasing
	glfwWindowHint(GLFW_SAMPLES, 4);

	//need at least OpenGl 4.3 for compute shader
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	//enable forward-compability and core profile
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	//define depth/stencil buffer
	glfwWindowHint(GLFW_DEPTH_BITS, 0);
	glfwWindowHint(GLFW_STENCIL_BITS, 0);

	//set window to not resizable
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	//create application window
	window = glfwCreateWindow(sizeX, sizeY, name.c_str(), NULL, NULL);

	if (!window)
	{
		glfwTerminate();
		return false;
	}

	// makes the context of our window current aka this thread is now OpenGL rendering thread
	glfwMakeContextCurrent(window);

	//load pointers to OpenGL functions/extensions at runtime
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	//set swap to 1
	glfwSwapInterval(1);

	//set vertex data of texture quad
	rt_texture_quad = new VertexData[4];

	rt_texture_quad[0].position[0] = -1.0;
	rt_texture_quad[0].position[1] = 1.0;
	rt_texture_quad[0].position[2] = 0.0;

	rt_texture_quad[1].position[0] = -1.0;
	rt_texture_quad[1].position[1] = -1.0;
	rt_texture_quad[1].position[2] = 0.0;

	rt_texture_quad[2].position[0] = 1.0;
	rt_texture_quad[2].position[1] = 1.0;
	rt_texture_quad[2].position[2] = 0.0;

	rt_texture_quad[3].position[0] = 1.0;
	rt_texture_quad[3].position[1] = -1.0;
	rt_texture_quad[3].position[2] = 0.0;

	//-------------------------------init UserData----------------------------------------------------------------
	//allocate memory for user data
	//userData = new UserData;

	//Not necessary
	//glfwSetWindowUserPointer(window, (void*)userData);

	//set vertex data count
	vertexDataCount = 4;

	//-------------------------------init Non-Compute Shaders + Program--------------------------------------------------------
	//Create the vertex shader:
	char* vertexShaderSource = readShaderFromFile("Shader/vertex.glsl");
	GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vertexShaderSource, "Vertex shader");

	delete(vertexShaderSource);


	//Create the fragment shader:
	char* fragmentShaderSource = readShaderFromFile("Shader/fragment.glsl");
	GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentShaderSource, "Fragment shader");

	delete(fragmentShaderSource);

	// hint to release resources of shader compiler
	glReleaseShaderCompiler();

	//program handle
	GLuint programObject;

	//create empty program object
	programObject = glCreateProgram();

	if (programObject == 0) return 0;

	// attach shaders to program object
	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	// link shaders
	glLinkProgram(programObject);
	checkError("glLinkProgram");

	// detach shaders from program object
	glDetachShader(programObject, vertexShader);
	glDetachShader(programObject, fragmentShader);

	// delete shader objects
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	if (!checkLinkedProgram(programObject))
	{
		return GL_FALSE;
	}

	// use program
	glUseProgram(programObject);
	checkError("glUseProgram");

	// store program object in context
	programObjectNonCompute = programObject;

	//initialize the modell
	time = glfwGetTime();

	//-------------------------------init VertexArray + Buffers----------------------------------------------------------------
	// create and bind dummy vao... it is needed in desktop OpenGL
	glGenVertexArrays(1, &(vertexArrayObject));
	checkError("glGenVertexArray");

	glBindVertexArray(vertexArrayObject);
	checkError("glBindVertexArray");

	// generate a VBO
	glGenBuffers(1, &(vertexBufferObject));
	checkError("glGenBuffers");

	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	checkError("glBindBuffer");

	//set buffer data
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData) * vertexDataCount, (const GLvoid*)rt_texture_quad, GL_STATIC_DRAW);
	checkError("glBufferData");

	//attribute index, component count, normalize, stride, pointer
	glVertexAttribPointer(ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, position));
	checkError("glVertexAttribPointer - ATTRIB_POSITION");

	//enable vertex attribute
	glEnableVertexAttribArray(ATTRIB_POSITION);
	checkError("glEnableVertexAttribArray - ATTRIB_POSITION");

	//attribute index, component count, normalize, stride, pointer
	glVertexAttribPointer(ATTRIB_TEX, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, texCoords));
	checkError("glVertexAttribPointer - ATTRIB_TEXTURE");

	//enable vertex attribute
	glEnableVertexAttribArray(ATTRIB_TEX);
	checkError("glEnableVertexAttribArray - ATTRIB_TEXTURE");

	// get the window size
	glfwGetFramebufferSize(window, &windowWidth, &windowHeight);

	// set viewport
	glViewport(0, 0, windowWidth, windowHeight);
	checkError("glViewport");

	// define clear for color buffer with black color
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
	checkError("glClearColor");

	generateTexture();

	queryWorkGroups();

	initComputeShader();

	return GL_TRUE;
}

void rt::rt_window::teardown()
{
	glDeleteBuffers(1, &(vertexBufferObject));
	checkError("glDeleteBuffers");

	glDeleteVertexArrays(1, &(vertexArrayObject));
	checkError("glDeleteVertexArrays");
}

void rt::rt_window::error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

void rt::rt_window::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
}

void rt::rt_window::checkError(const char* errorText) {
	GLenum error = glGetError();

	if (error != GL_NO_ERROR) {
		printf("GLError: %s - %d\n", errorText, error);
		exit(0);
	}
}

//http://antongerdelan.net/opengl/compute.html
void rt::rt_window::generateTexture()
{
	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);

	//set texture clamp to border
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//set pixel filter to linear
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//Texture type, LOD (base), internal pixel format, width, height, const 0, format loaded array, format loaded array
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, windowWidth, windowHeight, 0, GL_RGBA, GL_FLOAT,
		//data pointer
		NULL);

	//index, texture, LOD, layered, layer, access, format
	glBindImageTexture(0, texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
}

void rt::rt_window::queryWorkGroups() 
{
	int work_grp_size[3], work_grp_inv;
	// maximum global work group (total work in a dispatch)
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &work_grp_size[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &work_grp_size[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &work_grp_size[2]);
	printf("max global (total) work group size x:%i y:%i z:%i\n", work_grp_size[0],
		work_grp_size[1], work_grp_size[2]);
	// maximum local work group (one shader's slice)
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &work_grp_size[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &work_grp_size[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &work_grp_size[2]);
	printf("max local (in one shader) work group sizes x:%i y:%i z:%i\n",
		work_grp_size[0], work_grp_size[1], work_grp_size[2]);
	// maximum compute shader invocations (x * y * z)
	glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &work_grp_inv);
	printf("max computer shader invocations %i\n", work_grp_inv);
}

void rt::rt_window::initComputeShader()
{
	char* computeShaderSource = readShaderFromFile("Shader/compute.glsl");

	GLuint ray_shader = glCreateShader(GL_COMPUTE_SHADER);
	glShaderSource(ray_shader, 1, &computeShaderSource, NULL);
	glCompileShader(ray_shader);
	checkError("compile compute shader");

	delete(computeShaderSource);

	GLuint computeProgram = glCreateProgram();
	glAttachShader(computeProgram, ray_shader);
	glLinkProgram(computeProgram);
	checkError("linking program");

	programObjectCompute = computeProgram;
}

void rt::rt_window::update()
{
	//using compute shader to do the raytracing bit
	glUseProgram(programObjectCompute);
	glDispatchCompute((GLuint)windowWidth, (GLuint)windowHeight, 1);

	//image needs to be written before it is used
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	/*
	//calculate time delta
	double newTime = glfwGetTime();
	double timeDelta = newTime - userData->time;
	userData->time = newTime;

	userData->angleX = fmod(userData->angleX + (X_ROT_SPEED * timeDelta), 2 * M_PI);

	//update angle y:
	//Fmod = float modulo, [0.0, 2pi]
	userData->angleY = fmod(userData->angleY + (Y_ROT_SPEED * timeDelta), 2 * M_PI);
	*/
 
}

void rt::rt_window::draw()
{
	// clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	// use program
	glUseProgram(programObjectNonCompute);
	checkError("glUseProgram");

	glBindVertexArray(vertexArrayObject);

	//set finished texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	// draw stuff
	// primitive type, start index in array, number of elements to render
	//glDrawArrays(GL_TRIANGLE_FAN, 0, vertexDataCount);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, vertexDataCount);
	checkError("glDrawArrays");
}

char* rt::rt_window::readShaderFromFile(std::string filename) {
	char* result;
	std::stringstream shaderCodeStream;
	std::string codeLine;
	std::ifstream shaderFile;
	shaderFile.open(filename);

	if (shaderFile.is_open())
	{
		while (getline(shaderFile, codeLine))
		{
			shaderCodeStream << codeLine << '\n';
		}
		shaderFile.close();
	}

	std::string shaderCode = shaderCodeStream.str();

	result = new char[shaderCode.length() + 1];
	strcpy_s(result, shaderCode.length() + 1, shaderCode.c_str());

	return result;
}

GLboolean rt::rt_window::checkLinkedProgram(GLuint programObject)
{
	// link status
	GLint linked;

	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);
	checkError("glGetProgramiv");

	if (!linked) {

		GLint infoLen;

		//get length of linker error message
		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1) {
			char* errorMsg = new char[infoLen];

			//get error message from program object
			glGetProgramInfoLog(programObject, infoLen, NULL, errorMsg);

			printf("Error linking program: %s\n", errorMsg);
		}

		glDeleteProgram(programObject);

		return GL_FALSE;
	}
	return GL_TRUE;
}

GLuint rt::rt_window::compileShader(GLenum type, const char* shaderSource, std::string shaderTag) {

	//shader handle
	GLuint shader;

	//create empty shader object
	shader = glCreateShader(type);

	if (shader == 0) return 0;

	//put source code into empty shader object
	glShaderSource(shader, 1, &shaderSource, NULL);

	//compile shader
	glCompileShader(shader);

	//compile status
	GLint compiled;

	//check compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if (!compiled) {

		GLint infoLen = 0;

		//get length of error message
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1) {
			char* errorMsg = new char[infoLen];

			//get error message
			glGetShaderInfoLog(shader, infoLen, NULL, errorMsg);

			std::cout << "Error compiling shader " << shaderTag;
			printf(": %s\n", errorMsg);

		}

		// delete shader object
		glDeleteShader(shader);

		return GL_FALSE;
	}

	return shader;
}